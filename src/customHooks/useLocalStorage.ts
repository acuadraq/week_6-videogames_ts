import { useState } from 'react';

type Local = {
  id: number;
  token: string;
  username: string;
};

const getSavedData = (): Local | boolean => {
  if (localStorage.user) {
    const savedData = JSON.parse(localStorage.getItem('user') || '');
    return savedData;
  }
  return false;
};

const useAuth = (state?: boolean) => {
  const [value, setValue] = useState<Local | boolean>(() => getSavedData());
  if (state) setValue(false);
  return [value, setValue];
};

export default useAuth;
