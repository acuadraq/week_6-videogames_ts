import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import GamesList from './components/GamesList';
import Account from './components/Account';
import GameDetail from './components/GameDetail';
import NotFound from './components/NotFound';
import PublicRoute from './routers/PublicRoute';

const App = () => {
  return (
    <Router>
      <Navbar />
      <section>
        <div className="container">
          <Switch>
            <Route exact path="/" component={GamesList} />
            <PublicRoute path="/account" component={Account} />
            <Route path="/games/:id" component={GameDetail} />
            <Route component={NotFound} />
          </Switch>
        </div>
      </section>
      <Footer />
    </Router>
  );
};

export default App;
