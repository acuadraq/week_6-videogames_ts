import React, { Component, Props } from 'react';
import { Route, Redirect } from 'react-router-dom';
import useAuth from '../customHooks/useLocalStorage';
import RoutesNames from '../interfaces/enums';

interface PropsPath {
  path: string;
  component: React.ComponentType;
}

// eslint-disable-next-line no-shadow
const PublicRoute = ({ path, component: Component }: PropsPath) => {
  const [login, setLogin] = useAuth();
  return (
    <Route path={path}>
      {login === false ? <Component /> : <Redirect to={RoutesNames.home} />}
    </Route>
  );
};
export default PublicRoute;
