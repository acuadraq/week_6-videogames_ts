/* eslint no-undef: 0 */
import { IComments, IVideoGame, IUser } from '../interfaces/common';

type userInfo = {
  identifier: string;
  password: string;
};

type commInfo = {
  body: string;
};

const fetchApi = async <T>(extension: string, config?: RequestInit): Promise<T> => {
  const request = await fetch(`https://trainee-gamerbox.herokuapp.com${extension}`, config);
  const response = await request.json();
  return response;
};

export const getGames = () => fetchApi<IVideoGame[]>('/games/');

export const getGame = (id: number) => fetchApi<IVideoGame>(`/games/${id}`);

export const getComments = (id: number) => fetchApi<IComments[]>(`/games/${id}/comments?_limit=30`);

export const postLogin = (userObj: userInfo) => {
  const settings: RequestInit = {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-type': 'application/json',
    },
    body: JSON.stringify(userObj),
  };
  return fetchApi<IUser>('/auth/local', settings);
};

export const postComments = (id: number, commentObj: commInfo, token: string) => {
  const settings: RequestInit = {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
      'Content-type': 'application/json',
    },
    body: JSON.stringify(commentObj),
  };
  return fetchApi(`/games/${id}/comment`, settings);
};
