import React from 'react';
import { Link } from 'react-router-dom';
import notfound from '../notfound.svg';
import back from '../back.png';

const NotFound = () => {
  return (
    <div className="notfound__page">
      <h2>Page Not Found</h2>
      <img src={notfound} alt="" height="400" width="400" />
      <Link to="/">
        <span>
          <img src={back} alt="" width="42" height="48" className="mr-4" />
        </span>
        Go back home
      </Link>
    </div>
  );
};

export default NotFound;
