/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { useState, useEffect } from 'react';

type Pages = {
  itemsPerPage: number;
  totalItems: number;
  paginate: (pageNumber: number) => void;
  currentPage: number;
};

const Pagination = ({ itemsPerPage, totalItems, paginate, currentPage }: Pages) => {
  const [pageNumbers, setPageNumbers] = useState<number[]>([]);
  const urlSearch = new URL(window.location.href);

  const pag = urlSearch.searchParams.get('page');
  if (pag && parseInt(pag, 10) !== currentPage) {
    paginate(parseInt(pag, 10));
  }

  useEffect(() => {
    const total = Math.ceil(totalItems / itemsPerPage);
    setPageNumbers(Array.from({ length: total }, (_, index) => index + 1));
  }, [totalItems, itemsPerPage]);

  return (
    <ul className="pagination">
      {pageNumbers.map(number => (
        <div key={number} role="none" onClick={() => paginate(number)}>
          {currentPage === number ? (
            <li className="currentPage">{number}</li>
          ) : (
            <li key={number}>{number}</li>
          )}
        </div>
      ))}
    </ul>
  );
};

export default Pagination;
