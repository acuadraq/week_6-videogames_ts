import React, { Reducer, useReducer, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { IVideoGame } from '../interfaces/common';
import { getGame } from '../adapters/Fetch';
import { Actions, State, SearchAction } from '../reducer/actions';
import ReducerAction from '../reducer/index';
import NotFound from './NotFound';
import shop from '../shop.png';
import GameComments from './GameComments';

type PathParams = {
  id: string;
};

const initialState: State<IVideoGame> = {
  isLoading: false,
  error: '',
  data: null,
};

const GameDetail = () => {
  const { id } = useParams<PathParams>();
  const defaultImg = 'https://via.placeholder.com/380x500';
  const [state, dispatch] = useReducer<Reducer<State<IVideoGame>, SearchAction<IVideoGame>>>(
    ReducerAction,
    initialState,
  );

  const { data: game, isLoading, error } = state;

  useEffect(() => {
    dispatch({ type: Actions.SET_LOADING });
    getGame(parseInt(id, 10))
      .then(resp => {
        dispatch({ type: Actions.SET_SUCCESS, payload: { data: resp } });
      })
      .catch(err => {
        dispatch({
          type: Actions.SET_ERROR,
          payload: { error: err },
        });
      });
  }, []);

  return (
    <>
      {isLoading && <div className="animation__loading" />}
      {error && <NotFound />}
      {game && (
        <>
          <div className="game-detail__container">
            <div>
              <h2>{game.name}</h2>
              <p className="game__info">
                <span className="font-bold">
                  Publisher:
                  {game.publishers.map(publisher => (
                    <span className="font-normal" key={publisher.id}>
                      {' '}
                      {publisher.name}
                    </span>
                  ))}
                </span>
                <span className="font-bold">
                  Release Year:
                  <span className="font-normal">{game.release_year}</span>
                </span>
                <span className="font-bold">
                  Genre:
                  <span className="font-normal">{game.genre.name}</span>
                </span>
              </p>
              <div className="img__container">
                <img
                  src={game.cover_art == null ? defaultImg : game.cover_art.formats.small.url}
                  alt={game.name}
                  width="380"
                  height="500"
                  className="game__image"
                />
              </div>
            </div>
            <div className="game__content">
              <span className="price-top">Price match guarantee</span>
              <p className="game__price">{`$${game.price}`}</p>
              <div className="free-return">
                <span>15-DAY FREE & EASY RETURNS</span>
                <p>If recieved today, the last day to return this item would be in 15 days</p>
              </div>
              <hr />
              <div className="select-div">
                <p>Compatible Plataform(s):</p>
                <select name="platforms" id="platforms">
                  {game.platforms.map(platform => (
                    <option value={platform.id} key={platform.id}>
                      {platform.name}
                    </option>
                  ))}
                </select>
                <p>Software Format:</p>
                <select name="software" id="software">
                  <option value="1">Physical</option>
                  <option value="2">Digital</option>
                </select>
              </div>
              <button type="button">
                <img src={shop} alt="Shopping Cart" width="24" height="24" />
                <span>Add to Cart</span>
              </button>
            </div>
          </div>
          <GameComments id={parseInt(id, 10)} />
        </>
      )}
    </>
  );
};

export default GameDetail;
