import React from 'react';
import { Link } from 'react-router-dom';
import { IVideoGame } from '../interfaces/common';
import RoutesNames from '../interfaces/enums';

type GameType = {
  game: IVideoGame;
};

const Games = ({ game }: GameType) => {
  const defaultImg = 'https://via.placeholder.com/320x400';
  const { name, cover_art, price, id } = game;
  return (
    <div className="game__card">
      <Link to={`${RoutesNames.gameDetail}${id}`}>
        <img
          src={cover_art !== null ? cover_art.formats.small.url : defaultImg}
          alt={name}
          height="400"
        />
        <div className="game__content">
          <h3>{name}</h3>
          <div className="game__footer flex-class">
            <button type="button">Buy</button>
            <span>{`$${price}`}</span>
          </div>
        </div>
      </Link>
    </div>
  );
};

export default Games;
