// eslint-disable-next-line object-curly-newline
import React, { useReducer, useEffect, Reducer, useState } from 'react';
import { IComments } from '../interfaces/common';
import { Actions, State, SearchAction } from '../reducer/actions';
import { getComments, postComments } from '../adapters/Fetch';
import Pagination from './Pagination';
import ReducerAction from '../reducer/index';
import user from '../user.png';
import useAuth from '../customHooks/useLocalStorage';

type GameID = {
  id: number;
};

const initialState: State<IComments[]> = {
  isLoading: false,
  error: '',
  data: null,
};

type CommentT = {
  body: string;
};

const GameComments = ({ id }: GameID) => {
  const [login, setLogin] = useAuth();
  const [state, dispatch] = useReducer<Reducer<State<IComments[]>, SearchAction<IComments[]>>>(
    ReducerAction,
    initialState,
  );
  const [content, setContent] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [commentsPerPage] = useState(4);

  const { data: comments, isLoading, error } = state;

  const indexOfLastComment = currentPage * commentsPerPage;
  const indexOfFirstComment = indexOfLastComment - commentsPerPage;
  let currentComments: IComments[] = [];

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    const newComment: CommentT = {
      body: content,
    };
    await postComments(id, newComment, typeof login === 'object' ? login.token : '');
    setContent('');
    retrieveComments();
  };

  const retrieveComments = () => {
    dispatch({ type: Actions.SET_LOADING });
    getComments(id)
      .then(resp => {
        dispatch({ type: Actions.SET_SUCCESS, payload: { data: resp } });
      })
      .catch(err => {
        dispatch({
          type: Actions.SET_ERROR,
          payload: { error: err },
        });
      });
  };

  const paginate = (pageNumber: number) => setCurrentPage(pageNumber);

  if (comments) currentComments = comments.slice(indexOfFirstComment, indexOfLastComment);

  useEffect(() => {
    retrieveComments();
  }, []);

  return (
    <>
      <div className="comments__container">
        <h2>Game Reviews</h2>
        <hr />
        <div className="comments__grid">
          <div>
            {isLoading && <div className="animation__loading" />}
            {error && <p>{error}</p>}
            {comments && (
              <>
                {currentComments.map(comment => (
                  <div key={comment.id} className="comment">
                    <div className="comment__header">
                      <span>
                        <img src={user} width="32" height="32" alt="User Icon" />
                      </span>
                      <span className="comment__user">{comment.user.username}</span>
                    </div>
                    <hr />
                    <p>{comment.body}</p>
                  </div>
                ))}
                <Pagination
                  itemsPerPage={commentsPerPage}
                  totalItems={comments.length}
                  paginate={paginate}
                  currentPage={currentPage}
                />
              </>
            )}
          </div>
          <div className="order-reverse">
            {login === false && <h3>You need to log in, in order to post a comment</h3>}
            {login !== false && (
              <div className="comment__box">
                <h3>
                  Welcome
                  {typeof login === 'object' ? ` ${login.username}` : ''}
                </h3>
                <form onSubmit={handleSubmit}>
                  <h4>Post a comment</h4>
                  <hr />
                  <label htmlFor="content">
                    Content
                    <textarea
                      name="content"
                      id="content"
                      required
                      placeholder="Content of the comment"
                      onChange={e => setContent(e.target.value)}
                      value={content}
                    />
                  </label>
                  <div className="button__container">
                    <button type="submit" className="button">
                      Post Comment
                    </button>
                  </div>
                </form>
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default GameComments;
