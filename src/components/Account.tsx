import React, { useState } from 'react';
import { postLogin } from '../adapters/Fetch';
import { IUser } from '../interfaces/common';

interface userObj {
  identifier: string;
  password: string;
}

interface userStorage {
  id: number;
  token: string;
  username: string;
}

const Account = () => {
  const [msgError, setMsgError] = useState<string>('');
  const [user, setUser] = useState<string>('');
  const [password, setPassword] = useState<string>('');

  const saveInfo = (userInfo: IUser) => {
    const userLocalInfo: userStorage = {
      id: userInfo.user.id,
      token: userInfo.jwt,
      username: userInfo.user.username,
    };
    localStorage.setItem('user', JSON.stringify(userLocalInfo));
    window.location.reload();
  };

  const HandleLogin = async (e: React.FormEvent) => {
    try {
      e.preventDefault();
      if (user.trim() === '') {
        setMsgError('User Field must be fill');
        return;
      }
      const objUser: userObj = {
        identifier: user,
        password,
      };
      const userInfo = await postLogin(objUser);
      saveInfo(userInfo);
    } catch (err) {
      setMsgError('Username or Password are incorrect');
    }
  };
  return (
    <div className="form__container">
      <h1>Log In</h1>
      <form onSubmit={HandleLogin}>
        <label htmlFor="user">
          User
          <input
            onChange={e => setUser(e.target.value)}
            type="text"
            name="user"
            id="user"
            placeholder="User"
            value={user}
            className="inputs"
            required
          />
        </label>
        <label htmlFor="password">
          Password
          <input
            onChange={e => setPassword(e.target.value)}
            type="password"
            name="password"
            id="password"
            placeholder="Password"
            value={password}
            className="inputs"
            required
          />
        </label>
        <div className="button__container">
          <button className="button" type="submit">
            Log In
          </button>
        </div>
      </form>
      {msgError !== '' && (
        <div className="errorMsgLogin">
          <p>{msgError}</p>
        </div>
      )}
    </div>
  );
};

export default Account;
