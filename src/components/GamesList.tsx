import React, { useState, useEffect, useRef } from 'react';
import { useHistory } from 'react-router';
import { getGames } from '../adapters/Fetch';
import { IVideoGame } from '../interfaces/common';
import Pagination from './Pagination';
import Games from './Games';

const GamesList = () => {
  const [loading, setLoading] = useState<boolean>(true);
  const [games, setGames] = useState<IVideoGame[]>([]);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const gamesPerPage = 8;
  const searchInput = useRef<HTMLInputElement | null>(null);
  const urlSearch = new URL(window.location.href);
  let gamesLength = games.length;
  const history = useHistory();

  const getList = async () => {
    const list = await getGames();
    await setGames(list);
  };

  const getCurrentGames = (): IVideoGame[] => {
    const params = urlSearch.searchParams.get('search');
    if (params) {
      const gamesFilter = games.filter(item => item.name.toLowerCase().includes(params));
      gamesLength = gamesFilter.length;
      return gamesFilter.slice(indexOfFirstGame, indexOfLastGame);
    }
    return games.slice(indexOfFirstGame, indexOfLastGame);
  };

  useEffect(() => {
    let mounted = true;
    getList().then(() => {
      if (mounted) {
        setLoading(false);
      }
    });
    return function cleanup() {
      mounted = false;
    };
  }, []);

  const handleSearch = () => {
    if (searchInput.current !== null) history.push(`?search=${searchInput.current.value}`);
    setCurrentPage(1);
  };

  const indexOfLastGame = currentPage * gamesPerPage;
  const indexOfFirstGame = indexOfLastGame - gamesPerPage;
  const currentGames = getCurrentGames();

  const paginate = (pageNumber: number) => {
    urlSearch.searchParams.set('page', pageNumber.toString());
    history.push(`${urlSearch.search}`);
    setCurrentPage(pageNumber);
  };

  return (
    <>
      <div className="home_top">
        <h2>GAMES AVAILABLE</h2>
        <div>
          <input
            type="text"
            ref={searchInput}
            name="search"
            id="search"
            placeholder="Search game"
          />
          <button type="button" onClick={handleSearch}>
            Search
          </button>
        </div>
      </div>
      <hr />
      {loading && <div className="animation__loading" />}
      {!loading && (
        <div className="games__container">
          {currentGames.length === 0 && <p>No games found</p>}
          {currentGames.map(game => (
            <Games game={game} key={game.id} />
          ))}
        </div>
      )}
      <Pagination
        itemsPerPage={gamesPerPage}
        totalItems={gamesLength}
        paginate={paginate}
        currentPage={currentPage}
      />
    </>
  );
};

export default GamesList;
