/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React from 'react';
import { Link } from 'react-router-dom';
import useAuth from '../customHooks/useLocalStorage';
import RoutesNames from '../interfaces/enums';

const Navbar = () => {
  const [login, setLogin] = useAuth();

  const logOut = () => {
    localStorage.clear();
    window.location.reload();
  };

  return (
    <header>
      <nav className="flex-class">
        <ul className="flex-class">
          <li className="navbar__title">
            <Link to={RoutesNames.home}>Applaudo Buy</Link>
          </li>
          <div>
            <li>
              <Link to={RoutesNames.home}>Home</Link>
            </li>
            {login === false && (
              <li>
                <Link to={RoutesNames.account}>Log In</Link>
              </li>
            )}
            {login !== false && <li onClick={() => logOut()}>Log Out</li>}
          </div>
        </ul>
      </nav>
    </header>
  );
};

export default Navbar;
