import React from 'react';
import { Link } from 'react-router-dom';
import RoutesNames from '../interfaces/enums';

const Footer = () => {
  return (
    <footer>
      <div className="footer__container flex-class">
        <div className="footer__title">
          <h1>
            <Link to={RoutesNames.home}>Applaudo Buy</Link>
          </h1>
        </div>
        <div className="footer__links flex-class">
          <div>
            <h4>
              <Link to={RoutesNames.home}>Home</Link>
            </h4>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
