export interface IVideoGame {
  id: number;
  name: string;
  genre: {
    id: number;
    name: string;
    'created_at': string;
    'updated_at': string;
  };
  'release_year': number;
  price: string;
  'created_at': string;
  'updated_at': string;
  'cover_art': {
    id: number;
    name: string;
    alternativeText: string;
    caption: string;
    width: number;
    height: number;
    formats: {
      small: {
        ext: string;
        url: string;
        hash: string;
        mime: string;
        name: string;
        path: string | null;
        size: number;
        width: number;
        height: number;
        'provider_metadata': {
          'public_id': string;
          'resource_type': string;
        };
      };
      medium: {
        ext: string;
        url: string;
        hash: string;
        mime: string;
        name: string;
        path: string | null;
        size: number;
        width: number;
        height: number;
        'provider_metadata': {
          'public_id': string;
          'resource_type': string;
        };
      };
      thumbnail: {
        ext: string;
        url: string;
        hash: string;
        mime: string;
        name: string;
        path: string | null;
        size: number;
        width: number;
        height: number;
        'provider_metadata': {
          'public_id': string;
          'resource_type': string;
        };
      };
    };
    hash: string;
    ext: string;
    mime: string;
    size: number;
    url: string;
    previewUrl: string | null;
    provider: string;
    'provider_metadata': {
      'public_id': string;
      'resource_type': string;
    };
    'created_at': string;
    'updated_at': string;
  } | null;
  platforms: {
    id: 1;
    name: string;
    'created_at': string;
    'updated_at': string;
    logo: string | null;
  }[];
  publishers: {
    id: number;
    name: string;
    'created_at': string;
    'updated_at': string;
  }[];
  comments: {
    id: number;
    trainee: number | null;
    game: number;
    body: string;
    'created_at': string;
    'updated_at': string;
    user: number;
  }[];
}

export interface IComments {
  id: number;
  trainee: number | null;
  game: {
    id: number;
    name: string;
    genre: number;
    'release_year': number;
    price: number;
    'created_at': string;
    'updated_at': string;
    'cover_art': {
      id: number;
      name: string;
      alternativeText: string;
      caption: string;
      width: number;
      height: number;
      formats: {
        small: {
          ext: string;
          url: string;
          hash: string;
          mime: string;
          name: string;
          path: string | null;
          size: number;
          width: number;
          height: number;
          'provider_metadata': {
            'public_id': string;
            'resource_type': string;
          };
        };
        medium: {
          ext: string;
          url: string;
          hash: string;
          mime: string;
          name: string;
          path: string | null;
          size: number;
          width: number;
          height: number;
          'provider_metadata': {
            'public_id': string;
            'resource_type': string;
          };
        };
        thumbnail: {
          ext: string;
          url: string;
          hash: string;
          mime: string;
          name: string;
          path: string | null;
          size: number;
          width: number;
          height: number;
          'provider_metadata': {
            'public_id': string;
            'resource_type': string;
          };
        };
      };
      hash: string;
      ext: string;
      mime: string;
      size: number;
      url: string;
      previewUrl: string | null;
      provider: string;
      'provider_metadata': {
        'public_id': string;
        'resource_type': string;
      };
      'created_at': string;
      'updated_at': string;
    } | null;
  };
  body: string;
  'created_at': string;
  'updated_at': string;
  user: {
    id: number;
    username: string;
    email: string;
    provider: string;
    confirmed: boolean;
    blocked: boolean;
    role: number;
    'created_at': string;
    'updated_at': string;
    firstName: string;
    lastName: string;
  };
}

export interface IUser {
  jwt: string;
  user: {
    id: number;
    username: string;
    email: string;
    provider: string;
    confirmed: boolean;
    blocked: boolean;
    role: {
      id: number;
      name: string;
      description: string;
      type: string;
    };
    'created_at': string;
    'updated_at': string;
    firstName: string;
    lastName: string;
    comments: {
      id: number;
      trainee: number | null;
      game: number;
      body: string;
      'created_at': string;
      'updated_at': string;
      user: number;
    }[];
  };
}
