/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */

enum RoutesNames {
  home = '/',
  gameDetail = '/games/',
  account = '/account/',
}

export default RoutesNames;
