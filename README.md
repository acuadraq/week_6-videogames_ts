# Week_6 Videogames with Typescript

![](public/Screenshot.png)

## Description

This is the weekly assignment for the six week. Its a videogames store. This are the main points of the page:

- This website is using React and Typescript
- Use of react router dome for the site pages and links.
- Use of URL params to the game details like /games/2.
- URL query params for pagination and search game.
- Definition of interfaces for api responses.
- Enums for the route names.
- Use of useReducer and definition on initial state type.
- Custom hook to interact with localStorage

If you want to tested in real time use this url: https://week-6-videogames-ts.vercel.app/
